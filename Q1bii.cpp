#include "opencv2/opencv.hpp"
#include <iostream>
#include <cmath>
#include <string>
using namespace cv;
using namespace std;

int main( int argc, char** argv )
{
    if( argc != 3)
    {
     cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
    }

    Mat image;
    image = imread(argv[1], IMREAD_GRAYSCALE);	// Read the file
    if(! image.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }
    string imgname = argv[2];
    
	const int numPix = image.rows * image.cols;
	int count[numPix] = {0};
	double p[numPix] = {0};
	int lut[numPix] = {0};
	double s;
	for(int i = 0; i < image.rows; i++)
		for(int j = 0 ; j < image.cols; j++)
			count[image.at<uchar>(i,j)]++;
			
	for(int i = 0; i < numPix;i++)
		p[i] = count[i] / double(numPix);
	
	for(int i = 0; i < numPix;i++)
	{
		s += p[i];
		lut[i] = 255 * s;
    }
    
	Mat outImg = Mat(image.rows,image.cols,image.type(),double(0));
	
	for(int i = 0; i < image.rows; i++)
		for(int j = 0 ; j < image.cols; j++)
			outImg.at<uchar>(i,j) =  lut[image.at<uchar>(i,j)];
	
    
    imwrite("./"+imgname+".jpg",outImg);
    
    
    imshow( "After", outImg);                   // Show our image inside it.
    imshow( "Before", image);                   // Show our image inside it.


    waitKey(0);					 // Wait for a keystroke in the window
    return 0;
}

