#include "opencv2/opencv.hpp"
#include <iostream>
#include <cmath>
#include <string>
using namespace cv;
using namespace std;

double pi =  3.14159265;

int main( int argc, char** argv )
{
    if( argc != 10)
    {
     cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
    }

    Mat image;
    image = imread(argv[1], IMREAD_GRAYSCALE);	// Read the file
    if(! image.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }
   
    //Recieivng the input from the user 
    float sx = atof(argv[2]);
    float sy = atof(argv[3]);
    float tx = atof(argv[4]);
    float ty = atof(argv[5]);
    float theta = atof(argv[6]);
    float px = atof(argv[7]);
    float py = atof(argv[8]);
    string imgname = argv[9];
    
    //Doing the scaling first 
    int r = image.rows * sx;
    int c = image.cols * sy;
    
    Mat outImg = Mat(r,c,image.type());
	outImg = 0;
	for(int i = 0; i < r; i++)
		for(int j = 0; j < c; j++)
		{
			
			int x = i / sx;
			int y = j / sy;

			outImg.at<uchar>(i,j) = image.at<uchar>(x,y);
			
		}
    
    //Based on The Rotation & Transformation Matrices Dervation
    Mat tranTransRotate = Mat(3,3,CV_64F,double(0));
    tranTransRotate.at<double>(0,0) = cos(theta*(pi/180));
    tranTransRotate.at<double>(0,1) = -1 * sin(theta*(pi/180));
	tranTransRotate.at<double>(0,2) = -1 * px * cos(theta*(pi/180)) + py*sin(theta * (pi/180)) + px + tx;
	tranTransRotate.at<double>(1,0) = sin(theta*(pi/180));
    tranTransRotate.at<double>(1,1) = cos(theta*(pi/180));
	tranTransRotate.at<double>(1,2) = -1 * px * sin(theta*(pi/180)) - py*cos(theta * (pi/180)) + py + ty;
	tranTransRotate.at<double>(2,2) = 1;


	Mat outfin = Mat(r,c,image.type(),double(0));
	
	  //evaluating the invers of the matrix so we would loop on the output instead of the input
	  for(int i = 0; i < r; i++)
		for(int j = 0; j < c;j++)
		{
			Mat pix = Mat(3,1,CV_64F,double(1));
			pix.at<double>(0,0) = i;
			pix.at<double>(1,0) = j;
			Mat result = tranTransRotate.inv(DECOMP_LU)* pix;
			int x = result.at<double>(0,0);
			int y = result.at<double>(1,0);
			if( x >= 0 && x < r && y >= 0 && y < c)
				outfin.at<uchar>(i,j) = outImg.at<uchar>(x,y);
		}
    
    
    
    
    imwrite("./"+imgname+".jpg",outfin);
    
    
    imshow( "Display window", outfin);                   // Show our image inside it.

    waitKey(0);					 // Wait for a keystroke in the window
    return 0;
}
