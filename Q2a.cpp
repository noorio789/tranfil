#include "opencv2/opencv.hpp"
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
using namespace cv;
using namespace std;

//Smoothing the image

int main( int argc, char** argv )
{
    if( argc != 3)
    {
     cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
    }

    Mat image;
    image = imread(argv[1], IMREAD_GRAYSCALE);	// Read the file
    if(! image.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }
    
    string imgname = argv[2];
    
	Mat outImg = Mat(image.rows+2,image.cols+2,image.type(),double(0));
	
	
	//filling the frame with a duplicate of neihbourhood
	//for the first col 
	for(int i= 1; i <= image.rows; i++)
		{outImg.at<uchar>(i,0) = image.at<uchar>(i-1,0);
		//cout << (int)outImg.at<uchar>(i,0) << ' '  << (int) image.at<uchar>(i-1,0) << endl;
	}
	//for the last col 
	for(int i= 1; i <= image.rows; i++)
		outImg.at<uchar>(i,image.cols+1) = image.at<uchar>(i-1,image.cols-1);
	
	//filling the first row
	for(int i= 1; i <= image.cols; i++)
		outImg.at<uchar>(0,i) = image.at<uchar>(0,i-1);
	
	//filling the last col
	for(int i= 1; i <= image.cols; i++)
		outImg.at<uchar>(image.rows+1,i) = image.at<uchar>(image.rows-1,i-1);
	
	// filling the corners 
	outImg.at<uchar>(0,0) = (outImg.at<uchar>(0,1) + outImg.at<uchar>(1,0)) / double(2);
	outImg.at<uchar>(image.rows+1,image.cols+1) = (outImg.at<uchar>(image.rows+1,outImg.cols) + outImg.at<uchar>(image.rows,image.cols+1)) / double(2);
	outImg.at<uchar>(0,image.cols+1) = (outImg.at<uchar>(0,image.cols) + outImg.at<uchar>(1,image.cols+1)) / double(2);
	outImg.at<uchar>(image.rows+1,0) = (outImg.at<uchar>(image.rows,0) + outImg.at<uchar>(image.rows+1,1)) / double(2);

	for(int i = 1; i <= image.rows; i++)
		for(int j = 1 ; j <= image.cols; j++)
			outImg.at<uchar>(i,j) =  image.at<uchar>(i-1,j-1);
			
		
	double filter[9];
	double avg = 0;
	
	for(int x = 0; x < 9; x++)
		filter[x] = 1.0;
				
	for(int i = 1; i <= image.rows; i++)
		for(int j = 1 ; j <= image.cols; j++)
		{
			avg = 0;
			for(int x = 0; x < 3; x++)
				for(int y =0; y < 3; y++)
					avg += filter[x*3+y] * outImg.at<uchar>(i+x-1,j+y-1);
					//cout << avg << endl;
					
			outImg.at<uchar>(i,j) = avg/9;
			
		}
    
    imwrite("./"+imgname+".jpg",outImg);
    
    
    imshow( "Display window", outImg);                   // Show our image inside it.

    waitKey(0);					 // Wait for a keystroke in the window
    return 0;
}

