#include "opencv2/opencv.hpp"
#include <iostream>
#include <cmath>
#include <string>
using namespace cv;
using namespace std;

double pi =  3.14159265;

//The Nth power law 
int main( int argc, char** argv )
{
    if( argc != 4)
    {
     cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
    }

    Mat image;
    image = imread(argv[1], IMREAD_GRAYSCALE);	// Read the file
    if(! image.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }
    
    float gamma = atof(argv[2]);
    string imgname = argv[3];
    
    double c = ( 255 / pow(255,gamma));
    
	Mat outImg = Mat(image.rows,image.cols,image.type(),double(0));
	
	for(int i = 0; i < image.rows; i++)
		for(int j = 0 ; j < image.cols; j++)
			outImg.at<uchar>(i,j) =  c* pow(image.at<uchar>(i,j),gamma);
	
    
    imwrite("./"+imgname+".jpg",outImg);
    
    
    imshow( "Display window", outImg);                   // Show our image inside it.

    waitKey(0);					 // Wait for a keystroke in the window
    return 0;
}
