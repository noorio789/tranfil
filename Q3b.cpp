#include "opencv2/opencv.hpp"
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
#include <ctime>
#include <cstdio>
#include <chrono>

using namespace cv;
using namespace std;
using namespace std::chrono;

//The site I used to caculate the timings 
//This code is done for median filter as it is seperable 

int main( int argc, char** argv )
{	
		
    if( argc != 4)
    {
     cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
    }

    Mat image;
    image = imread(argv[1], IMREAD_GRAYSCALE);	// Read the file
    if(! image.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }
    
    string imgname1 = argv[2];
    string imgname2 = argv[3];
    
    float fnum;
    
    cout << " Enter Filter Size : " ;
    cin >> fnum;
    
	Mat inter = Mat(image.rows+(fnum/2),image.cols+(fnum/2),image.type(),double(0));
	Mat out2D = Mat(image.rows,image.cols,image.type(),double(0));
	Mat out1D = Mat(image.rows,image.cols,image.type(),double(0));
 
	for(int i = fnum/2; i <= image.rows; i++)
		for(int j = fnum/2 ; j <= image.cols; j++)
			inter.at<uchar>(i,j) =  image.at<uchar>(i-fnum/2,j-fnum/2);
			
		
	double Twofilter[(const int) pow(fnum,2)];
	double firstFilter[(const int) fnum];
	double avg = 0;
	
	for(int x = 0; x < (const int) pow(fnum,2); x++)
		Twofilter[x] = 1.0;
				
	for(int x = 0; x < (const int) fnum; x++)
		firstFilter[x] = 1.0;
		
	high_resolution_clock::time_point twoD1 = high_resolution_clock::now();
	//2D Filter 	
	for(int i = fnum/2; i <= image.rows; i++)
		for(int j = fnum/2 ; j <= image.cols; j++)
		{
			avg = 0;
			for(int x = 0; x < (int)fnum; x++)
				for(int y =0; y < (int)fnum; y++)
					avg += Twofilter[x*(int)fnum+y] * inter.at<uchar>(i+x-(fnum/2),j+y-(fnum/2));
					
			out2D.at<uchar>(i-fnum/2,j-fnum/2) = avg/(pow(fnum,2));
			
		}
	high_resolution_clock::time_point twoD2 = high_resolution_clock::now();
	
	double avg1 = 0;
	double avg2 = 0;
	
	high_resolution_clock::time_point oneD1 = high_resolution_clock::now();
	//1D Filter
	for(int i = fnum/2; i <= image.rows; i++)
		for(int j = fnum/2 ; j <= image.cols; j++)
		{
			avg1 = 0; 
			for(int x = 0; x < (int) fnum; x++)
					avg1 += firstFilter[x] * inter.at<uchar>(i,j+x-(fnum/2));
	
			avg1 /= fnum;
			inter.at<uchar>(i,j) = avg1;

		}
    
    for(int i = fnum/2; i <= image.rows; i++)
		for(int j = fnum/2 ; j <= image.cols; j++)
		{
			avg2 =0;
				for(int x = 0; x < (int) fnum; x++)
					avg2 += firstFilter[x] * inter.at<uchar>(i+x-(fnum/2),j);
					
				avg2 /= fnum;
			
			out1D.at<uchar>(i-fnum/2,j-fnum/2) = avg2;
					
		}
	high_resolution_clock::time_point oneD2 = high_resolution_clock::now();

	
	auto d1 = duration_cast<microseconds>( twoD2 - twoD1 ).count();
	auto d2 = duration_cast<microseconds>( oneD2 - oneD1 ).count();


	cout << "Execution time of 2D" << (int) fnum << "filter: " << d1 << endl;
	cout << "Execution time of 1D" << (int) fnum << "filter: " << d2 << endl;
	
    imwrite("./"+imgname1+".jpg",out1D);
    imwrite("./"+imgname2+".jpg",out2D);
    
    imshow( "1D", out1D);                   // Show our image inside it.
    imshow( "2D", out2D);                   // Show our image inside it.

    waitKey(0);					 // Wait for a keystroke in the window
    return 0;
}


