# README #

Description: 
------------
This Assignment is implementing the linear transformations for images and also implementing filters such as blurring, sharping and Gradient extraction

Notice:
---------
Please Open the Pdf of the assignment to match each source code with a meaningful requirement. Also, Open the pdf Report for the result and for compilation format.


Screenshots -
------------------------------------

![alt text](https://bytebucket.org/noorio789/tranfil/raw/62373e6bbac835aa6a7767db8c86ea59a5817b0d/Images%20Produced/test2DF11.jpg "Smoothing with 11x11 Filter")

![alt text](https://bytebucket.org/noorio789/tranfil/raw/62373e6bbac835aa6a7767db8c86ea59a5817b0d/Images%20Produced/testHE.jpg "Histogramic Equalization")

![alt text](https://bytebucket.org/noorio789/tranfil/raw/62373e6bbac835aa6a7767db8c86ea59a5817b0d/Images%20Produced/testSharp.jpg "Sharpening Filter")

![alt text](https://bytebucket.org/noorio789/tranfil/raw/9d778c56b4d19aabb27307b810b05a853cb8fd10/Images%20Produced/homeGred.jpg "Gradient Filter for Edge Extraction")

![alt text](https://bytebucket.org/noorio789/tranfil/raw/62373e6bbac835aa6a7767db8c86ea59a5817b0d/Images%20Produced/homeScaled3translated25rotated25around112%2C112.jpg "Linear Transformation application")

![alt text](https://bytebucket.org/noorio789/tranfil/raw/62373e6bbac835aa6a7767db8c86ea59a5817b0d/Images%20Produced/homeGamma3.jpg "Gamma Effect")



