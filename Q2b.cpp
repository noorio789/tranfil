#include "opencv2/opencv.hpp"
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
using namespace cv;
using namespace std;

//Gradient the image : NOTE: I apply Prewitt Operators

int main( int argc, char** argv )
{
    if( argc != 3)
    {
     cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
    }

    Mat image;
    image = imread(argv[1], IMREAD_GRAYSCALE);	// Read the file
    if(! image.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }
    
    string imgname = argv[2];
    
	Mat outImg = Mat(image.rows+2,image.cols+2,image.type(),double(0));
	Mat GxImg = Mat(image.rows+2,image.cols+2,image.type(),double(0));
	Mat GyImg = Mat(image.rows+2,image.cols+2,image.type(),double(0));
	Mat out = Mat(image.rows+2,image.cols+2,image.type(),double(0));
	
	//filling the frame with a duplicate of neihbourhood
	//for the first col 
	for(int i= 1; i <= image.rows; i++)
		{outImg.at<uchar>(i,0) = image.at<uchar>(i-1,0);
		//cout << (int)outImg.at<uchar>(i,0) << ' '  << (int) image.at<uchar>(i-1,0) << endl;
	}
	//for the last col 
	for(int i= 1; i <= image.rows; i++)
		outImg.at<uchar>(i,image.cols+1) = image.at<uchar>(i-1,image.cols-1);
	
	//filling the first row
	for(int i= 1; i <= image.cols; i++)
		outImg.at<uchar>(0,i) = image.at<uchar>(0,i-1);
	
	//filling the last col
	for(int i= 1; i <= image.cols; i++)
		outImg.at<uchar>(image.rows+1,i) = image.at<uchar>(image.rows-1,i-1);
	
	// filling the corners 
	outImg.at<uchar>(0,0) = (outImg.at<uchar>(0,1) + outImg.at<uchar>(1,0)) / double(2);
	outImg.at<uchar>(image.rows+1,image.cols+1) = (outImg.at<uchar>(image.rows+1,outImg.cols) + outImg.at<uchar>(image.rows,image.cols+1)) / double(2);
	outImg.at<uchar>(0,image.cols+1) = (outImg.at<uchar>(0,image.cols) + outImg.at<uchar>(1,image.cols+1)) / double(2);
	outImg.at<uchar>(image.rows+1,0) = (outImg.at<uchar>(image.rows,0) + outImg.at<uchar>(image.rows+1,1)) / double(2);

	
	for(int i = 1; i <= image.rows; i++)
		for(int j = 1 ; j <= image.cols; j++)
			outImg.at<uchar>(i,j) =  image.at<uchar>(i-1,j-1);
			
	//For Extracting the vertical component 
	int Gx[9]={-1,0,1,-1,0,1,-1,0,1};
	//For Extracting the horizontal component
	int Gy[9] ={-1,-1,-1,0,0,0,1,1,1};
	int gx = 0;
	int gy =0;
	
				
	for(int i = 1; i < image.rows+1; i++)
		for(int j = 1 ; j < image.cols+1; j++)
		{
			gx = 0; gy = 0;
			for(int x = 0; x < 3; x++)
				for(int y =0; y < 3; y++)
					{
						gx += Gx[x*3+y] * outImg.at<uchar>(i+x-1,j+y-1);
						gy += Gy[x*3+y] * outImg.at<uchar>(i+x-1,j+y-1);
					}
					
			//normalize the pixel 		
			if( gx < 0 )
					gx = 0;
			if(gy < 0)
					gy = 0;
			if(gx > 255)
					gx = 255;
			if(gy > 255)
					gy = 255;
			
			GxImg.at<uchar>(i,j) = gx;
			GyImg.at<uchar>(i,j) = gy;
			out.at<uchar>(i,j) = sqrt(pow(gx,2)+pow(gy,2));
			
		}
    
    imwrite("./"+imgname+".jpg",out);
    
    
    imshow( "Out", out);                   // Show our image inside it.
	imshow( "Gx", GxImg); 
	imshow( "Gy", GyImg); 
    waitKey(0);					 // Wait for a keystroke in the window
    return 0;
}


